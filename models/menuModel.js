const mongoose = require("mongoose");

var menuSchema = mongoose.Schema({
    foodname: String,
    desc: String,
    price: String,
    status: String,
    image: String,
    creation_date: {
        type: Date,
        default: Date.now,
    }
});

module.exports = mongoose.model("Menu", menuSchema);