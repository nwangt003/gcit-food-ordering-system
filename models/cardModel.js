const mongoose = require("mongoose")

const cartSchema = mongoose.Schema({
    userId: String,
    menuItemId: String
})

module.exports = new mongoose.model("Cart", cartSchema)