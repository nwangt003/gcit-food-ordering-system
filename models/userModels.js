const mongoose = require('mongoose')
const validator = require('validator')

const userSchema = new mongoose.Schema({
    name: {
        type:String,
        required:[true, 'Please tell your name!'], 
    },
    email:{
        type:String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'Please provide a valid email'],
    },
    password:{
        type:String,
        required: [true, 'Please provide a password'],
        minlenght:8,
        select:false,
    },
    active:{
        type:Boolean,
        default:true,
        select:false,
    },
})

const User = mongoose.model('User', userSchema)
module.exports = User