//jshint esversion:6
require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");
const app = express();
const path = require('path');
const cookieParser = require("cookie-parser");

const userRoute = require("./routes/user");
const userMenuRoute = require("./routes/userCartMenuRoutes")
const orderRoute = require("./routes/orderRoutes")

app.use(express.static("public"));
app.use(express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(express.json());

app.use(bodyParser.urlencoded({
  extended: true
}));

//set up mongoDB connection
mongoose.connect(process.env.MONGO_ATLAS, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connected to mongo.");
  })
  .catch((err) => {
    console.log("Error connecting to mongo.", err);
  });

app.use("/", userRoute);
app.use("/", userMenuRoute);
app.use("/", orderRoute);

let port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server Listening at http://localhost:${port}`);
});
