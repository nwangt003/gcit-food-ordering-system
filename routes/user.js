//jshint esversion:6

const express = require('express');
const router = express.Router();
const bcrypt = require("bcrypt");
const crypto = require("crypto");
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const User = require("../models/userModel");
const Menu = require('../models/menuModel');
const app = express();
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const {
  loginrequired,
  verifyEmail
} = require("../config/JWT");
const {
  registerValidation,
  loginValidation
} = require("../validator/validation");
const {check,
sanitizedBody,
  matchedData,
  validationResult
} = require("express-validator");
const { render } = require('ejs');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads');
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toISOString +(file.originalname));
  }
});

var upload = multer({
  storage: storage,
  // fileFilter: fileFilter,
});

router.get("/", function(req, res) {
  res.render("index");
});

router.get("/login", function(req, res) {
  res.render("login");
});

router.get("/signup", function(req, res) {
  res.render("signup");
});

router.get("/secrets", loginrequired, function(req, res) {
  res.render("secrets");
});

router.get("/resetPasswordForm", function(req, res){
  res.render("resetPasswordForm");
});

router.get("/forgotPassword", function(req, res){
  res.render("forgotPassword");
});

router.get("/admindash", function(req, res){
  res.render("admindash")
})

router.get("/about", function(req, res){
  res.render("about")
})

router.get("/addmenu", function(req, res){
  res.render("addmenu")
})

router.get("/editmenu", function(req, res){
  res.render("editmenu")
})


router.get("/userabout", function(req, res) {
  res.render("userabout");
});

// router.get("/updatemenu", function(req, res){
//   res.render("updatemenu")
// })

// mail sender details
var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: process.env.auth_user,
    pass: process.env.auth_pass
  },
  tls: {
    rejectUnauthorized: false
  }
});

router.get("/logout", function(req, res, next) {
  res.cookie("access_token", "", {
    maxAge: 1
  });
  res.redirect("/login");
});

const createToken = (id) => {
  return jwt.sign({
    id
  }, process.env.JWT_SECRET);
};
router.get("/verify-email", function(req, res) {
  try {
    const token = req.query.token;
    const user = User.findOne({
      emailToken: token
    });
    if (user) {
      user.updateOne({
        isVerified: true
      }, function(err) {
        if (err) {
          res.render("login", {
            errorMessage: err
          });
        } else {
          res.render("login", {
            successMessage: "Your Email has been successfully verified, Please login to continue..."
          });
        }
      });
    } else {
      res.render("signup", {
        errorMessage: "Something went wrong, Please try with different gmail!"
      });
    }
  } catch (err) {
    console.log("Verification Failed here " + err);
    res.render("login", {
      errorMessage: err
    });
  }
});
var token1;
router.get("/verify-password", function(req, res) {
  const token = req.query.token;
  token1 = token;
  const user = User.findOne({
    emailToken: token
  });
  if(user){
    res.render("resetPasswordForm");
  }
});

router.post("/resetPasswordForm", [//Password validation
check("password").trim().notEmpty().withMessage("Password is required!").isLength({
  min: 5
}).withMessage("Password must be minimum 5 characters long"),
//Confirm password validation
check("confirmPassword").custom((value, {
  req
}) => {
  if (value !== req.body.confirmPassword) {
    throw new Error("Password confimation didn't match");
  }
  return true;
})], function(req, res) {
  try {
    var errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errMsg = errors.mapped();
      var inputData = matchedData(req);
      res.render("resetPasswordForm", {
        errors: errMsg,
        inputData: inputData
      });
    } else {
      console.log("TOKKEEEEN " + token1);
      const user = User.findOne({
        emailToken: token1
      });
      console.log("USERNAME "+ user);
      if (user) {
        bcrypt.hash(req.body.password, 10, function(err, hash){
          user.updateOne({password: hash}, function(err){
            if (err) {
              res.render("resetPasswordForm", {
                errorMessage: err
              });
            } else {
              res.render("login", {
                successMessage: "You have successfully reset your new password, Please login to continue..."
              });
            }
          });
        });
      }
    }
  } catch (err) {
    console.log("Verification Failed here " + err);
    res.render("login", {
      errorMessage: err
    });
  }
});

router.post("/signup", registerValidation, function(req, res) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errMsg = errors.mapped();
      var inputData = matchedData(req);
      res.render("signup", {
        errors: errMsg,
        inputData: inputData
      });
    } else {
      User.findOne({
        email: req.body.email
      }, function(err, foundUser) {
        if (err) {
          res.render("signup", {
            errorMessage: err
          });
        } else if (foundUser) {
          res.render("signup", {
            errorMessage: "This email is already used by another person. Please try with different email."
          });
        } else {
          bcrypt.hash(req.body.password, 10, function(err, hash) {
            const user = new User({
              name: req.body.name,
              email: req.body.email,
              phone: req.body.phone,
              password: hash,
              emailToken: crypto.randomBytes(64).toString("hex"),
              isVerified: false,
            });
            user.save(function(err) {
              if (err) {
                console.log(err);
              } else {
                link = "http://" + req.headers.host + "/verify-email?token=" + user.emailToken;
                var mailOptions = {
                  from: "GCIT Food Ordering System",
                  to: user.email,
                  subject: "GCIT Food Ordering System - Verify Your Email",
                  html: "<h2>Hello " + req.body.name + ", Thanks for registering on our Website</h2><h4> Please verify your email to continue...</h4><a href=" + link + ">Verify your Email</a>"
                };

                //sending mail
                transporter.sendMail(mailOptions, function(error, info) {
                  if (error) {
                    console.log("email" + error);
                  } else {
                    console.log("Verification link is sent to your gmail account");
                    res.render("login", {
                      successMessage: "Verification link is sent to your gmail account"
                    });
                  }
                });
              }
            });
          });
        }
      });
    }
  } catch (err) {
    console.log("Verification Invalid" + err);
    res.render("signup", {
      errorMessage: "Something went wrong, Please try again!"
    });
  }
});

router.post("/login", loginValidation, verifyEmail,
  function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errMsg = errors.mapped();
      var inputData = matchedData(req);
      res.render("login", {
        errors: errMsg,
        inputData: inputData
      });
    } else {
      const email = req.body.email;
      const password = req.body.password;
      if(email === "kijungty123@gmail.com"){
        res.render("admindash")
      }else{
        User.findOne({
          email: email
        }, function(err, foundUser) {
          if (err) {
            console.log(err);
            res.render("login", {
              errorMessage: err
            });
          } else if (foundUser) {
            bcrypt.compare(password, foundUser.password, function(err, result) {
              if (result === true) {
                const token = createToken(foundUser._id);
                //store token in cookies
                res.cookie("access_token", token);
                res.render("secrets");
              } else {
                res.render("login", {
                  errorMessage: "Your password is incorrect!"
                });
              }
            });
          } else {
            res.render("login", {
              errorMessage: "No such User found!"
            });
          }
        });
      }
    }
  });

router.post("/forgotPassword", [check("email").notEmpty().withMessage("Email Address is required!").normalizeEmail().isEmail().withMessage("Email address must be valid")],
  function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      var errMsg = errors.mapped();
      var inputData = matchedData(req);
      res.render("forgotPassword", {
        errors: errMsg,
        inputData: inputData
      });
    } else {
      User.findOne({
        email: req.body.email
      }, function(err, foundUser) {
        if (err) {
          console.log(err);
          res.render("forgotPassword", {
            errorMessage: err
          });
        } else if (foundUser) {
          link = "http://" + req.headers.host + "/verify-password?token=" + foundUser.emailToken;
          var mailOptions = {
            from: "GCIT Food Ordering System",
            to: req.body.email,
            subject: "GCIT Food Ordering System - Reset Password",
            html: "<h2>Hello " + foundUser.name + ",</h2><br><h4>Please click the link given below to reset forgot password.</h4><br><a href=" + link + ">Click here</a>"
          };

          //sending mail
          transporter.sendMail(mailOptions, function(error, info) {
            if (error) {
              res.render("forgotPassword", {
                errorMessage: error + " No such User found!"
              });
            } else {
              console.log("Verification link is sent to your gmail account");
              res.render("forgotPassword", {
                successMessage: "Reset Verification link has been successfully to your register email."
              });
            }
          });
        } else {
          res.render("forgotPassword", {
            errorMessage: "No such User found!"
          });
        }
      });
    }
  });

//Get Image Data
router.get("/menudetails", (req, res) => {
    Menu.find({}, (err, allmenus) => {
        if (err) {
            res.redirect('/menudetails')

        } else {
            res.render('menudetails', {
                menus: allmenus.reverse(), successMessage: "Order Successfull"
            });
        }
    });
});

router.post("/menudetails", upload.single('image'), (req, res, next) => {
  //add new menu
  var foodname = req.body.foodname;
  var desc = req.body.desc;
  var price = req.body.price;
  var status = req.body.status;
  console.log("HHHHHHHH"+req.file.filename);
  
  var newMenu = {
    foodname: foodname,
    desc: desc,
    price: price,
    status: status,
    image: req.file.filename
  };
  //Save to database
  Menu.create(newMenu, (err, newlyCreated) => {
    if (err) {
        console.log("Error in inserting into DB");
    } else {
        res.redirect('/menudetails');
    }
  });
})

// //SHOW - render show template with given id
// router.get("/:id", function(req, res) {
//   //find the post with provided id
//   Menu.findById(req.params.id)
//       .populate("comments")
//       .exec((err, foundMenu) => {
//           if (err) {
//               console.log(err);
//           } else {

//               //render show template with that post
//               res.render('menudetails', { menus: foundMenu });

//               console.log(foundMenu);
//           }
//       });
// });


//=================EDIT POST ROUTE=====================
router.get("/updatemenu/:id", function(req, res) {
  Menu.findById(req.params.id, (err, foundMenu) => {
      res.render("updatemenu", { menu: foundMenu });
  });
});

//UPDATE POST ROUTE
router.post("/updatemenu/:id", function(req, res) {

  var foodname = req.body.foodname;
  var desc = req.body.desc;
  var price = req.body.price;
  Menu.findOne({_id: req.params.id}, function(err, foundMenu){
    if(err){
      console.log();
    }
    if(foundMenu){
      var img = foundMenu.image;
      console.log("IIII"+img);
      Menu.updateOne({_id: req.params.id},{foodname: foodname,desc: desc, price: price,image: img}, function(err){
        if(err){
          console.log();
        }else{
          res.redirect('../menudetails');
        }
      })
    }
  })
});

//DESTROY POST ROUTE
router.get("/delete/:id", (req, res) => {

  Menu.findByIdAndRemove(req.params.id, function (err, menu) {
    if (err) {
    
    res.redirect('../menudetails');
    } else {
      res.redirect('../menudetails');
    }
  });
});

router.get("/customerdetails", function(req, res){
  User.find({ "users": { $ne: null } }, function (err, foundUser) {
    if (err) {

    } else {
      console.log("i am here");
        res.render("customerdetails",{users: foundUser })
    }
  })
})

module.exports = router;
