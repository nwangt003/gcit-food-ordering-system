const express = require('express');
const router = express.Router();
const Menu = require('../models/menuModel');
const Cart = require("../models/cardModel")
const userModel = require("../models/userModel")
const {transporter} = require("../config/email");

router.get("/orderstatus", function (req, res) {
    Cart.find({ "carts": { $ne: null } }, function (err, foundCartItems) {
        if (err) {

        } else {
            Menu.find({ "menus": { $ne: null } }, function (err, foundMenus) {
                if (err) {

                } else {
                    userModel.find({ "menus": { $ne: null } }, function (err, foundusers) {
                        if (err) {

                        } else {
                            res.render("orderstatus", { menus: foundMenus, cartItems: foundCartItems, users: foundusers });
                        }
                    })
                }
            })
        }
    })
})

router.post("/orderstatus", function (req, res) {
    userModel.findOne({ _id: req.body.accepted }, function (err, foundUser) {
        if (err) {
            console.log();
        }
        if (foundUser) {
            Cart.find({ "carts": { $ne: null } }, function (err, foundCartItems) {
                if (err) {

                } else {
                    Menu.find({ "menus": { $ne: null } }, function (err, foundMenus) {
                        if (err) {

                        } else {
                            userModel.find({ "menus": { $ne: null } }, function (err, foundusers) {
                                if (err) {

                                } else {
                                    var mailOptions = {
                                        from: process.env.auth_user,
                                        to: foundUser.email,
                                        subject: "Request Accepted",
                                        html: "Your order has been accepted."
                                    };
                                    //sending mail
                                    transporter.sendMail(mailOptions, function(error, info) {
                                    if (error) {
                                        console.log("email" + error);
                                    } else {
                                        console.log("Email sent");
                                        res.render("orderstatus", { menus: foundMenus, cartItems: foundCartItems, users: foundusers });
                                    }
                                    });
                                    
                                }
                            })
                        }
                    })
                }
            })
        }
    })

})

//DESTROY POST ROUTE
router.get("/Canceled/:id", (req, res) => {
    userModel.findOne({ _id: req.params.id }, function (err, foundUser) {
        if (err) {
            console.log();
        }
        if (foundUser) {
            Cart.find({ "carts": { $ne: null } }, function (err, foundCartItems) {
                if (err) {

                } else {
                    Menu.find({ "menus": { $ne: null } }, function (err, foundMenus) {
                        if (err) {

                        } else {
                            userModel.find({ "menus": { $ne: null } }, function (err, foundusers) {
                                if (err) {

                                } else {
                                    var mailOptions = {
                                        from: process.env.auth_user,
                                        to: foundUser.email,
                                        subject: "Request Canceled",
                                        html: "Your order has been canceled."
                                    };
                                    //sending mail
                                    transporter.sendMail(mailOptions, function(error, info) {
                                    if (error) {
                                        console.log("email" + error);
                                    } else {
                                        console.log("Canceled Email sent");
                                        res.redirect("../orderstatus");
                                    }
                                    });
                                    
                                }
                            })
                        }
                    })
                }
            })
        }
    })
});


module.exports = router;