const express = require('express');
const router = express.Router();
const Menu = require('../models/menuModel');
const Cart = require("../models/cardModel")
const cookie = require("cookie-parser");
const jwt = require('jsonwebtoken');
const userModel = require("../models/userModel")

router.get("/userMenu", (req, res) => {
    Menu.find({"menus": {$ne: null}}, function(err, foundMenus){
      if(err){
        res.render("userMenu", {errorMessage: err});
      }else{
        res.render("userMenu", {menus: foundMenus});
      }
    });
});

//DESTROY POST ROUTE
router.post("/userMenu", (req, res) => {
  const token = req.cookies['access_token'];
  var submit = req.body.submit;
  if(token){
    const validatetoken = jwt.verify(token, process.env.JWT_SECRET);
    if(validatetoken){
      console.log("UID"+validatetoken.id);
       const carts = Cart({
        userId: validatetoken.id,
        menuItemId: submit
      })
      carts.save(function(err){
        if(err){
          console.log(err);
        }else{
          console.log("Added");
          Menu.find({"menus": {$ne: null}}, function(err, foundMenus){
            if(err){
              res.render("userMenu", {errorMessage: err});
            }else{
              res.render("userMenu", {menus: foundMenus, successMessage: "Ordered Successfully"});
            }
          });
        }
      })
    }else{
      console.log("token expires");
      res.render("login", {errorMessage: "You are logout"});
    }
  }else{
    console.log("token not found!");
    res.render("login", {errorMessage: "Please login"});
  }
});

router.get("/userCartItem", function(req, res){
  const token = req.cookies['access_token'];
  if(token){
    const validatetoken = jwt.verify(token, process.env.JWT_SECRET);
    if(validatetoken){
      console.log("UID"+validatetoken.id);
      Cart.find({"carts":{$ne: null}}, function(err, foundCartItems){
        if(err){
            
        }else{
          Menu.find({"menus": {$ne: null}}, function(err, foundMenus){
            if(err){
              res.render("userMenu", {errorMessage: err});
            }else{
              foundCartItems.forEach(function(item){
                foundMenus.forEach(function(menu){
                  if(item.userId === validatetoken.id && menu._id == item.menuItemId){
                    console.log("Menu"+menu._id);
                  }
                })
              })
              res.render("userCartItem", {menus: foundMenus, cartItems: foundCartItems, uid: validatetoken.id});
            }
          });
        }
      })
    }else{
      console.log("token expires");
      res.render("login", {errorMessage: "You are logout"});
    }
  }else{
    console.log("token not found!");
    res.render("login", {errorMessage: "Please login"});
  }
})

module.exports = router;