# GCIT Food Ordering System

An online web platform that enables GCIT students and staff to select the food items of their choice from the menu list and order food in the canteen. It mainly provides customers better and fast services without time consuming, making it more effective and convenient to use. 

Built using: HTML/CSS/JS, Bootstrap, Express.js, Node.js, MongoDB.

## Project Team Members

Product Owner: Karma Yangzom (12190058)

<img src="Images/12190058.jpg" width="200" height="230"> 


Scrum Master: Samten Wangmo (12190073)

<img src="Images/73.jpg" width="200" height="230">


Developer: Ngawang Choden (12190066)

<img src="Images/66.jpg" width="200" height="230">


Developer: Gyeltshen Wangchuk(12190052)

<img src="Images/52.jpg" width="200" height="230">


Developer: Dawa Tshering (12190046)

<img src="Images/46.jpg" width="200" height="230">




